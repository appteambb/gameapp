package com.bb.above1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.text.TextPaint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements Runnable {

    GameActivity gameActivity;
    boolean playing = true;

    private int screenX;
    private int screenY;
    private float debugX = 0;
    private float debugY = 0;

    private Thread gameThread = null;
    private SensorManager mySensorManager;
    private Runner runner;
    private Grass grass;
    private ObstacleHandler obsHandler;

    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;
    private int score = 0;

    public GameView(Context context, int screenX, int screenY, GameActivity gameActivity) {
        super(context);
        this.screenX = screenX;
        this.screenY = screenY;
        this.gameActivity = gameActivity;

        runner = new Runner(context, screenX, screenY);
        grass = new Grass(context, screenX, screenY, runner);
        obsHandler = new ObstacleHandler(runner,context,grass,screenX,screenY,this);

        surfaceHolder = getHolder();
        paint = new Paint();

        mySensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        Sensor rotSensor = mySensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);

        mySensorManager.registerListener(
                RotSensorListener,
                rotSensor,
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void run() {
        while (playing) {
            update();
            draw();
            control();
        }
    }

    private void update() {
        runner.update();

        grass.update();

        obsHandler.update();

        for (Obstacle obs:obsHandler.getAktivObstacles()) {
            if (Rect.intersects(runner.getCollisionBox(), obs.getCollisionBox())) {
                playing = false;
                System.out.println("HIT");
                gameActivity.gameOver();
            }
        }
    }

    private void draw() {
        if (surfaceHolder.getSurface().isValid()) {
            canvas = surfaceHolder.lockCanvas();

            canvas.translate(0,canvas.getHeight());
            canvas.scale(1,-1);

            canvas.drawColor(Color.GREEN);

            canvas.drawBitmap(
                    grass.getBitmap1(),
                    grass.getX(),
                    grass.getY1(),
                    paint);

            canvas.drawBitmap(
                    grass.getBitmap2(),
                    grass.getX(),
                    grass.getY2(),
                    paint);

            canvas.drawBitmap(
                    grass.getBitmap3(),
                    grass.getX(),
                    grass.getY3(),
                    paint);

            for (Obstacle obs:obsHandler.getAktivObstacles()) {
                canvas.drawBitmap(obs.getImage(), obs.getX(), obs.getY(), paint);
                //canvas.drawRect(obs.getCollisionBox(), paint);
            }

            canvas.drawBitmap(
                    runner.getBitmap(),
                    runner.getX(),
                    runner.getY(),
                    paint);

            TextPaint textPaint = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
            textPaint.setColor(Color.WHITE);
            textPaint.setTextSize(80);

            canvas.save();
            canvas.scale(1,-1);
            canvas.drawText("Score: "+score, 0, -screenY+100, textPaint);
            canvas.drawText(debugX + "   " +debugY, 0, -screenY+200, textPaint);
            canvas.restore();

            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void control() {
        try {
            gameThread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void pause() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
        }
    }

    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    private final SensorEventListener RotSensorListener
            = new SensorEventListener(){

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            float xAxis = (float) (Math.round(event.values[1]*100.0)/100.0);
            float yAxis = (float) (Math.round(event.values[0]*100.0)/100.0);

            debugX = xAxis;
            debugY = yAxis;

            if (yAxis < -0.25){
                runner.setSpeed(25);
            } else if (yAxis < -0.2){
                runner.setSpeed(20);
            } else if (yAxis < -0.1){
                runner.setSpeed(10);
            } else if (yAxis < -0.05){
                runner.setSpeed(5);
            } else if (yAxis > 0.15){
                runner.setSpeed(-15);
            } else if (yAxis > 0.1){
                runner.setSpeed(-10);
            } else if (yAxis > 0.05) {
                runner.setSpeed(-5);
            } else {
                runner.setSpeed(0);
            }

            if (xAxis < -0.15){
                runner.setSpeedX(-15);
            } else if (xAxis > 0.15){
                runner.setSpeedX(15);
            }else {
                runner.setSpeedX(0);
            }
        }

    };

    public boolean isPlaying() {
        return playing;
    }
    public int getScore(){
        return score;
    }
    public void setScore(int score){
        this.score = score;
    }
}