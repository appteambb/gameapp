package com.bb.above1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Grass {
    private Bitmap bitmap1;
    private Bitmap bitmap2;
    private Bitmap bitmap3;
    private int x;
    private int y1;
    private int y2;
    private int y3;
    private int speed = 0;
    private int maxY;
    private int minY;
    private Runner runner;

    public Grass(Context context, int screenX, int screenY, Runner runner) {
        speed = 10;
        x = 0;

        y1 = 0;
        bitmap1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.grass);

        y2 = bitmap1.getHeight();
        bitmap2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.grass);

        y3 = bitmap2.getHeight()+y2;
        bitmap3 = BitmapFactory.decodeResource(context.getResources(), R.drawable.grass);

        maxY = screenY - bitmap1.getHeight();

        minY = 0;

        this.runner = runner;
    }

    public void update() {
        speed = 10 + runner.getSpeed();

        if (y1 <= 0 && y1 - speed > 0) {
            y1 = 0;
            speed = 0;
            y2 = bitmap1.getHeight();
            y3 = bitmap2.getHeight()+y2;

        } else if (y2 <= 0 && y2 - speed > 0) {
            y2 = 0;
            speed = 0;
            y3 = bitmap2.getHeight();
            y1 = bitmap3.getHeight()+y3;

        } else if (y3 <= 0 && y3 - speed > 0) {
            y3 = 0;
            speed = 0;
            y1 = bitmap3.getHeight();
            y2 = bitmap1.getHeight()+y1;

        } else {
            y1 = y1 - speed;
            y2 = y2 - speed;
            y3 = y3 - speed;

            if (bitmap1.getHeight() + y1 < 0) {
                y1 = bitmap3.getHeight() + y3;
            }

            if (bitmap2.getHeight() + y2 < 0) {
                y2 = bitmap1.getHeight() + y1;
            }

            if (bitmap3.getHeight() + y3 < 0) {
                y3 = bitmap2.getHeight() + y2;
            }
        }
    }

    public Bitmap getBitmap1() {
        return bitmap1;
    }

    public int getY1() {
        return y1;
    }

    public Bitmap getBitmap2() {
        return bitmap2;
    }

    public int getY2() {
        return y2;
    }
    public Bitmap getBitmap3() {
        return bitmap3;
    }

    public int getY3() {
        return y3;
    }

    public int getX() {
        return x;
    }

    public int getSpeed() {
        return speed;
    }
}
