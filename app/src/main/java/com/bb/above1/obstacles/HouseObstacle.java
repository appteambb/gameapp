package com.bb.above1.obstacles;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.bb.above1.Obstacle;
import com.bb.above1.R;
import com.bb.above1.Runner;

public class HouseObstacle extends Obstacle {
    public HouseObstacle(Context context, int x, int y, int screenX, int screenY, Runner runner) {
        image = BitmapFactory.decodeResource(context.getResources(), R.drawable.house);
        speed = 10;
        this.x = x;
        this.y = y;
        this.screenX = screenX;
        this.screenY = screenY;
        collisionBox =  new Rect(x, y, image.getWidth(), image.getHeight());
        this.runner = runner;
    }
}
