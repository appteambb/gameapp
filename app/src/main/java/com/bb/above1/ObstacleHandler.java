package com.bb.above1;

import android.content.Context;

import com.bb.above1.obstacles.CarObstacle;
import com.bb.above1.obstacles.HouseObstacle;
import com.bb.above1.obstacles.PoolObstacle;

import java.util.ArrayList;
import java.util.Random;

public class ObstacleHandler {
    private int screenX;
    private int screenY;
    private Runner runner;
    private Grass grass;
    private Context context;
    private GameView gameView;
    private ArrayList<Obstacle> aktivObstacles;
    private ArrayList<Obstacle> removeObstacles;
    private Random r;

    public ObstacleHandler(Runner runner, Context context, Grass grass, int screenX, int screenY, GameView gameView){
        this.screenX = screenX;
        this.screenY = screenY;
        this.runner = runner;
        this.context = context;
        this.grass = grass;
        this.gameView = gameView;

        aktivObstacles = new ArrayList<>();
        removeObstacles = new ArrayList<>();
        r = new Random();

        choosePreset();
        choosePreset();
    }

    private void choosePreset(){
        int random = r.nextInt(4 - 1) + 1;
        if (random == 1){
            addPoolToList(screenX/2,screenY/2);
            addCarToList(screenX/4,screenY/3);
            addHouseToList(0,screenY/3);
            addPoolToList(screenX/2,screenY/2);
            addCarToList(screenX/4,screenY/3);
            addHouseToList(screenX/4*3,screenY/3);
        } else if (random == 2){
            addHouseToList(screenX/4*3,screenY/2);
            addHouseToList(screenX/2,screenY/3);
            addHouseToList(0,0);
            addHouseToList(screenX/3,screenY/2);
            addHouseToList(screenX/4,screenY/3);
            addHouseToList(screenX,screenY/3);
        } else if (random == 3){
            addCarToList(screenX,screenY/2);
            addHouseToList(screenX/4,screenY/3);
            addCarToList(screenX/2,screenY/3);
            addHouseToList(0,screenY/3);
        }
    }

    private void addPoolToList(int x, int distanceY){
        if (aktivObstacles.isEmpty()){
            aktivObstacles.add(new PoolObstacle(context,x-100,screenY/2,screenX, screenY, runner));
        } else {
            aktivObstacles.add(new PoolObstacle(context,x-100,aktivObstacles.get(aktivObstacles.size()-1).getY()+distanceY,screenX, screenY, runner));
        }
    }

    private void addHouseToList(int x, int distanceY){
        if (aktivObstacles.isEmpty()){
            aktivObstacles.add(new HouseObstacle(context,x-60,screenY/2,screenX, screenY, runner));
        } else {
            aktivObstacles.add(new HouseObstacle(context,x-60,aktivObstacles.get(aktivObstacles.size()-1).getY()+distanceY,screenX, screenY, runner));
        }
    }

    private void addCarToList(int x, int distanceY){
        if (aktivObstacles.isEmpty()){
            aktivObstacles.add(new CarObstacle(context,x-70,screenY/2,screenX, screenY, runner));
        } else {
            aktivObstacles.add(new CarObstacle(context,x-70,aktivObstacles.get(aktivObstacles.size()-1).getY()+distanceY,screenX, screenY, runner));
        }
    }

    public void update() {
        for (Obstacle obs : aktivObstacles) {
            obs.setSpeed(10 + runner.getSpeed());
            obs.setY(obs.getY()-obs.getSpeed());

            if (obs.getY()< -512) {
                removeObstacles.add(obs);
            }

            obs.collisionBox.left = obs.getX();
            obs.collisionBox.right = obs.getX()+obs.image.getWidth();
            obs.collisionBox.top = obs.getY();
            obs.collisionBox.bottom = obs.getY()+obs.image.getHeight();
        }

        for (Obstacle obs : removeObstacles) {
            if (aktivObstacles.contains(obs)){
                aktivObstacles.remove(obs);
                gameView.setScore(gameView.getScore()+1);
            }
        }

        removeObstacles.clear();

        if (aktivObstacles.size() < 8){
            choosePreset();
        }
    }

    public ArrayList<Obstacle> getAktivObstacles() {
        return aktivObstacles;
    }
}
