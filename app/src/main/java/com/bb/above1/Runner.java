package com.bb.above1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Runner {
    private Bitmap bitmap;
    private int x;
    private int y;
    private int speed = 0;
    private int speedX = 0;
    private Rect collisionBox;

    private int maxX;
    private int minX;
    private int maxY;
    private int minY;

    private final int MIN_SPEED = -50;
    private final int MAX_SPEED = 50;

    public Runner(Context context, int screenX, int screenY) {
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.runnerv2);
        x = screenX/2-bitmap.getWidth()/2;
        y = 0;
        speed = 0;

        maxY = screenY-250;
        minY = 0;
        maxX = screenX-bitmap.getWidth();
        minX = 0;

        collisionBox =  new Rect(x, y, bitmap.getWidth(), bitmap.getHeight());
    }

    public void update() {

        if (speed > MAX_SPEED) {
            speed = MAX_SPEED;
        }

        if (speed < MIN_SPEED) {
            speed = MIN_SPEED;
        }


        x = x + speedX;
        y = y + speed;

        if (y < minY) {
            y = minY;
        }
        if (y > maxY) {
            y = maxY;
        }

        if (x < minX) {
            x = minX;
        }
        if (x > maxX) {
            x = maxX;
        }

        collisionBox.left = x;
        collisionBox.top = y;
        collisionBox.right = x + bitmap.getWidth();
        collisionBox.bottom = y + bitmap.getHeight();
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed){
        this.speed = speed;
    }

    public void setSpeedX(int speedX){
        this.speedX = speedX;
    }

    public Rect getCollisionBox() {
        return collisionBox;
    }
}
